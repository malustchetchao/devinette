import random

# Générer un nombre aléatoire entre 1 et 100
nombre_cible = random.randint(1, 100)

# Initialiser le nombre d'essais
essais = 0

# Définir le nombre maximum d'essais
max_essais = 10

print("Bienvenue au jeu de devinettes !")
print(f"Je choisis un nombre entre 1 et 100. À vous de deviner en moins de {max_essais} essais.")

while essais < max_essais:
    # Obtenir la devinette de l'utilisateur
    devinette = int(input("Entrez votre devinette : "))

    # Incrémenter le nombre d'essais
    essais += 1

    # Vérifier si la devinette est correcte
    if devinette == nombre_cible:
        print(f"Bravo ! Vous avez trouvé le nombre {nombre_cible} en {essais} essais.")
        break

if essais == max_essais:
    print(f"Désolé, vous n'avez pas trouvé le nombre. La réponse était {nombre_cible}.")
